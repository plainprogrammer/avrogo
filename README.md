# Avro.go

[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
[![pipeline status](https://gitlab.com/plainprogrammer/avrogo/badges/master/pipeline.svg)](https://gitlab.com/plainprogrammer/avrogo/commits/master)
[![coverage report](https://gitlab.com/plainprogrammer/avrogo/badges/master/coverage.svg)](https://gitlab.com/plainprogrammer/avrogo/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plainprogrammer/avrogo)](https://goreportcard.com/report/gitlab.com/plainprogrammer/avrogo)

A library for working with the [Apache Avro][avro] serialization format. This
library is intended to conform to version 1.8.2 of the [Avro specification]
[spec] and is released under an MIT license.

[avro]: http://avro.apache.org/
[spec]: http://avro.apache.org/docs/1.8.2/spec.html "Avro 1.8.2 Specification"
