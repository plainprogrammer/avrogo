package schema

import "encoding/json"

// Schema represents an Avro schema document as parsed from a JSON source
// representation. This representation includes all relevant details on the
// fields and other relevant structural elements needed for encoding, decoding,
// and validating Avro messages.
type Schema struct {
	Name      string
	Type      string
	Namespace string
}

// FromString parses a JSON-formatted string according to the expectations of
// an Avro schema document.
func FromString(schemaString string) *Schema {
	schema := new(Schema)
	json.Unmarshal([]byte(schemaString), schema)
	return schema
}
