package schema_test

import (
	"github.com/MakeNowJust/heredoc"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/plainprogrammer/avrogo/schema"
)

var _ = Describe("Avrogo: Schema", func() {
	Describe("FromString", func() {
		Context("with a minimal schema", func() {
			subject := schema.FromString(heredoc.Doc(`
				{"type": "record", "name": "OuterRecord", "fields": [
        			{"name": "field1", "type": {
          				"type": "record", "name": "InnerRecord", "fields": []
        			}},
        			{"name": "field2", "type": "InnerRecord"}
      			]}
			`))

			Describe("the top-level schema definition", func() {
				It("has its Name set to 'OuterRecord'", func() {
					Expect(subject.Name).To(Equal("OuterRecord"))
				})

				It("has its Type set to 'record'", func() {
					Expect(subject.Type).To(Equal("record"))
				})

				It("has its Namespace set to ''", func() {
					Expect(subject.Namespace).To(Equal(""))
				})
			})
		})
	})
})
